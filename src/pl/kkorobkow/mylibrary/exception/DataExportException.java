package pl.kkorobkow.mylibrary.exception;

public class DataExportException extends RuntimeException {
    public DataExportException(String message) {
        super(message);
    }
}
