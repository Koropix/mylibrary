package pl.kkorobkow.mylibrary.exception;

public class PublicationBorrowingException extends RuntimeException {
    public PublicationBorrowingException(String message) {
        super(message);
    }
}
