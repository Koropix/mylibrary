package pl.kkorobkow.mylibrary.exception;

public class UnreturnedPublicationException extends RuntimeException {
    public UnreturnedPublicationException(String message) {
        super(message);
    }
}
