package pl.kkorobkow.mylibrary.exception;

public class PublicationReturningException extends RuntimeException {
    public PublicationReturningException(String message) {
        super(message);
    }
}
