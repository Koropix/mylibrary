package pl.kkorobkow.mylibrary.exception;

public class SmallDataImportException extends RuntimeException {
    public SmallDataImportException(String message) {
        super(message);
    }
}
