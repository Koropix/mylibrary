package pl.kkorobkow.mylibrary.exception;

public class DataImportException extends RuntimeException {
    public DataImportException(String message) {
        super(message);
    }
}
