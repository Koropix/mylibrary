package pl.kkorobkow.mylibrary.model;

import pl.kkorobkow.mylibrary.exception.*;
import pl.kkorobkow.mylibrary.model.borrow.BorrowedPublication;
import pl.kkorobkow.mylibrary.model.borrow.Borrower;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

public class Library implements Serializable {
    private static final long serialVersionUID = 20200224;
    private Map<String, Publication> publications = new HashMap<>();
    private Map<Long, LibraryUser> libraryUsers = new HashMap<>();

    public Map<String, Publication> getPublications() {
        return publications;
    }

    public Map<Long, LibraryUser> getLibraryUsers() {
        return libraryUsers;
    }

    public Collection<Publication> getSortedPublications(Comparator<Publication> comparator) {
        return publications.values().stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public Collection<LibraryUser> getSortedLibraryUsers(Comparator<LibraryUser> comparator) {
        return libraryUsers.values().stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    public void addPublication(Publication publication) {
        String title = publication.getTitle();
        String key = publication.getKey();
        if (publications.containsKey(key))
            throw new PublicationAlreadyExistsException("W bazie danych istnieje już publikacja o tytule \"" + title + "\".");
        else
            publications.put(key, publication);
    }

    public void addLibraryUser(LibraryUser user) {
        long pesel = user.getPesel();
        if (libraryUsers.containsKey(pesel))
            throw new UserAlreadyExistsException("Użytkownik o nr. PESEL \"" + pesel + "\" już istnieje w bazie danych.");
        else
            libraryUsers.put(pesel, user);
    }

    public void removePublication(Publication publication) {
        if (publication.getBorrowers().isEmpty())
            publications.remove(publication.getKey());
        else
            throw new UnreturnedPublicationException("Publikacja nie została zwrócona przez co najmniej jednego " +
                    "czytelnika - nie można jej usunąć.");
    }

    public void removeLibraryUser(LibraryUser libraryUser) {
        if (libraryUser.getBorrowedPublications().isEmpty())
            libraryUsers.remove(libraryUser.getPesel());
        else
            throw new UnreturnedPublicationException("Czytelnik posiada niezwróconą publikację - nie można go usunąć.");
    }

    public Optional<Publication> findPublicationByKey(String typedPhrase) {
        String key = typedPhrase.toLowerCase();
        Publication publication = publications.get(key);
        return Optional.ofNullable(publication);
    }

    public Publication[] findPublicationsContainingPhrase(String typedPhrase) {
        return publications.values().stream()
                .filter(p -> p.getKey().contains(typedPhrase))
                .sorted()
                .toArray(Publication[]::new);
    }

    public Publication getPublicationFromOptionalByKey(String typedPhrase) {
        Optional<Publication> pubOpt = findPublicationByKey(typedPhrase);
        if (pubOpt.isPresent()) {
            return pubOpt.get();
        }
        throw new NoSuchElementException("Publikacja o tytule \"" + typedPhrase + "\" nie istnieje w bazie danych.");
    }

    public Optional<LibraryUser> findLibraryUserByPesel(Long pesel) {
        LibraryUser libraryUser = libraryUsers.get(pesel);
        return Optional.ofNullable(libraryUser);
    }

    public LibraryUser getLibraryUserFromOptionalByPesel(long pesel) {
        Optional<LibraryUser> userOpt = findLibraryUserByPesel(pesel);
        if (userOpt.isPresent()) {
            return userOpt.get();
        }
        throw new NoSuchElementException("Czytelnik o nr. PESEL \"" + pesel + "\" nie istnieje w bazie danych.");
    }

    public void borrowPublicationToUser(Publication pub, LibraryUser user) {
        boolean userHasNotReachedLimit = user.getBorrowedPublicationsNumber() < LibraryUser.BORROWED_PUBLICATIONS_LIMIT;
        boolean publicationStillAvailable = pub.getQuantity() > 0;

        if (!publicationStillAvailable) {
            throw new PublicationBorrowingException("Wszystkie egzemplarze publikacji \"" + pub.getTitle()
                    + "\" są już wypożyczone.");
        } else if (!userHasNotReachedLimit) {
            throw new PublicationBorrowingException("Użytkownik osiągnał limit możliwych do wypożyczenia publikacji ("
                    + LibraryUser.BORROWED_PUBLICATIONS_LIMIT + ").");
        } else {
            LocalDateTime actualTime = LocalDateTime.now();
            BorrowedPublication borrowedPublication = new BorrowedPublication(actualTime, pub);
            Borrower borrower = new Borrower(actualTime, user);

            user.borrowPublication(borrowedPublication);
            pub.addBorrower(borrower);
            pub.quantityReduce(1);
        }
    }

    public void returnPublicationFromUser(LibraryUser user, Publication pub) {
        BorrowedPublication borrowedPublication;
        Borrower borrower;

        try {
            borrowedPublication = user.getFirstBorrowedPublicationContainsPublication(pub);
            borrower = pub.getFirstBorrowerContainsLibraryUser(user);
            user.returnPublication(borrowedPublication);
            pub.removeBorrower(borrower);
            pub.quantityIncrease(1);
        } catch (NoSuchElementException e) {
            throw new PublicationReturningException("Użytkownik " + user + " nie posiada publikacji pt. \""
                    + pub.getTitle() + "\".");
        }
    }
}
