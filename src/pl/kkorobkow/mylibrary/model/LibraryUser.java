package pl.kkorobkow.mylibrary.model;

import pl.kkorobkow.mylibrary.model.borrow.BorrowedPublication;

import java.util.*;
import java.util.stream.Collectors;

public class LibraryUser extends User {
    public static final int BORROWED_PUBLICATIONS_LIMIT = 3;
    private long pesel;
    private List<BorrowedPublication> publicationHistory = new ArrayList<>();
    private List<BorrowedPublication> borrowedPublications = new ArrayList<>();

    public void borrowPublication(BorrowedPublication pub) {
        borrowedPublications.add(pub);
    }

    public void returnPublication(BorrowedPublication pub) {
        boolean result = borrowedPublications.remove(pub);
        if (result) {
            pub.setReturnTimeToNow();
            addPublicationToHistory(pub);
        }
    }

    private void addPublicationToHistory(BorrowedPublication pub) {
        publicationHistory.add(pub);
    }

    public BorrowedPublication getFirstBorrowedPublicationContainsPublication(Publication publication) {
        BorrowedPublication borrowedPublication = null;

        boolean borrowedPublicationFound = false;
        int i = 0;
        while (!borrowedPublicationFound && i < borrowedPublications.size()) {
            BorrowedPublication tmpBorrowedPublication = borrowedPublications.get(i);
            Publication tmpPublication = tmpBorrowedPublication.getPublication();
            if (tmpPublication.equals(publication)) {
                borrowedPublication = tmpBorrowedPublication;
                borrowedPublicationFound = true;
            } else i++;
        }

        if (borrowedPublication != null) return borrowedPublication;
        throw new NoSuchElementException();
    }

    public List<BorrowedPublication> getSortedBorrowedPublications() {
        return borrowedPublications.stream()
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
    }

    public List<BorrowedPublication> getSortedPublicationHistory() {
        return publicationHistory.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    public int getBorrowedPublicationsNumber() {
        return borrowedPublications.size();
    }

    public LibraryUser(String firstName, String lastName, long pesel) {
        super(firstName, lastName);
        this.pesel = pesel;
    }

    public long getPesel() {
        return pesel;
    }

    public void setPesel(long pesel) {
        this.pesel = pesel;
    }

    public List<BorrowedPublication> getPublicationHistory() {
        return publicationHistory;
    }

    public void setPublicationHistory(List<BorrowedPublication> publicationHistory) {
        this.publicationHistory = publicationHistory;
    }

    public List<BorrowedPublication> getBorrowedPublications() {
        return borrowedPublications;
    }

    public void setBorrowedPublications(List<BorrowedPublication> borrowedPublications) {
        this.borrowedPublications = borrowedPublications;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        LibraryUser that = (LibraryUser) o;
        return pesel == that.pesel &&
                Objects.equals(publicationHistory, that.publicationHistory) &&
                Objects.equals(borrowedPublications, that.borrowedPublications);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), pesel, publicationHistory, borrowedPublications);
    }

    @Override
    public String toString() {
        return super.toString() + " - PESEL: " + pesel;
    }
}
