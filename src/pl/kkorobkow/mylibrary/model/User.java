package pl.kkorobkow.mylibrary.model;

import java.io.Serializable;
import java.util.Objects;

public abstract class User implements Serializable, Comparable<User> {
    private static final long serialVersionUID = 20200213;
    private String firstName;
    private String lastName;

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(firstName, user.firstName) &&
                Objects.equals(lastName, user.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        return lastName + " " + firstName;
    }

    @Override
    public int compareTo(User u) {
        int lastNameCompare = lastName.compareToIgnoreCase(u.lastName);
        if (lastNameCompare != 0) return lastNameCompare;

        return firstName.compareToIgnoreCase(u.firstName);
    }
}
