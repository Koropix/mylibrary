package pl.kkorobkow.mylibrary.model;

import java.time.Year;
import java.util.Objects;

public class Book extends Publication {
    public static final String TYPE = "BOOK";
    private String author;
    private Year releasedYear;
    private int pagesNumber;

    public Book(String title, String publisher, int quantity, String author, Year releasedYear, int pagesNumber) {
        super(title, publisher, quantity);
        this.author = author;
        this.releasedYear = releasedYear;
        this.pagesNumber = pagesNumber;
    }

    public Book(String title, String publisher, int quantity, long ean, String author, Year releasedYear, int pagesNumber) {
        super(title, publisher, quantity, ean);
        this.author = author;
        this.releasedYear = releasedYear;
        this.pagesNumber = pagesNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return pagesNumber == book.pagesNumber &&
                Objects.equals(author, book.author) &&
                Objects.equals(releasedYear, book.releasedYear);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), author, releasedYear, pagesNumber);
    }

    @Override
    public String toString() {
        String withoutEan = super.toString() + " | autor: " + author + " | rok wydania: " + releasedYear
                + " | liczba stron: " + pagesNumber + " | ilość na magazynie: " + getQuantity() + " egz.";
        String withEan = withoutEan + " | kod EAN: " + getEan() + "]";
        if(getEan() == 0) return withoutEan + "]";
        return withEan;
    }
}
