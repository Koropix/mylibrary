package pl.kkorobkow.mylibrary.model.borrow;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

abstract class Borrow implements Serializable, Comparable<Borrow> {
    private static final DateTimeFormatter BORROW_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
    private LocalDateTime borrowTime;

    public Borrow(LocalDateTime borrowTime) {
        this.borrowTime = borrowTime;
    }

    public LocalDateTime getBorrowTime() {
        return borrowTime;
    }

    public void setBorrowTime(LocalDateTime borrowTime) {
        this.borrowTime = borrowTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Borrow borrow = (Borrow) o;
        return Objects.equals(borrowTime, borrow.borrowTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(borrowTime);
    }

    @Override
    public String toString() {
        return borrowTime.format(BORROW_TIME_FORMATTER);
    }

    @Override
    public int compareTo(Borrow b) {
        return borrowTime.compareTo(b.borrowTime);
    }


}
