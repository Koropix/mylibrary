package pl.kkorobkow.mylibrary.model.borrow;

import pl.kkorobkow.mylibrary.model.Publication;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class BorrowedPublication extends Borrow {
    private static final DateTimeFormatter SHORT_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private Publication publication;
    private LocalDateTime returnTime;

    public BorrowedPublication(LocalDateTime borrowTime, Publication publication) {
        super(borrowTime);
        this.publication = publication;
    }

    public Publication getPublication() {
        return publication;
    }

    public void setPublication(Publication publication) {
        this.publication = publication;
    }

    public LocalDateTime getReturnTime() {
        return returnTime;
    }

    public void setReturnTime(LocalDateTime returnTime) {
        this.returnTime = returnTime;
    }

    public void setReturnTimeToNow() {
        this.returnTime = LocalDateTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        BorrowedPublication that = (BorrowedPublication) o;
        return Objects.equals(publication, that.publication);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), publication);
    }

    @Override
    public String toString() {
        boolean isBorrowedPublicationReturned = returnTime != null;

        if (isBorrowedPublicationReturned) {
            return getBorrowTime().format(SHORT_TIME_FORMATTER) + '-' + returnTime.format(SHORT_TIME_FORMATTER)
                    + " -> " + publication;
        }
        return super.toString() + " -> " + publication;
    }
}
