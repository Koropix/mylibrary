package pl.kkorobkow.mylibrary.model.borrow;

import pl.kkorobkow.mylibrary.model.LibraryUser;

import java.time.LocalDateTime;
import java.util.Objects;

public class Borrower extends Borrow {
    private LibraryUser libraryUser;

    public Borrower(LocalDateTime borrowTime, LibraryUser libraryUser) {
        super(borrowTime);
        this.libraryUser = libraryUser;
    }

    public LibraryUser getLibraryUser() {
        return libraryUser;
    }

    public void setLibraryUser(LibraryUser libraryUser) {
        this.libraryUser = libraryUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Borrower borrower = (Borrower) o;
        return Objects.equals(libraryUser, borrower.libraryUser);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), libraryUser);
    }

    @Override
    public String toString() {
        return super.toString() + " -> " + libraryUser;
    }
}
