package pl.kkorobkow.mylibrary.model;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Magazine extends Publication {
    public static final String TYPE = "MAGAZINE";
    public static final String DATE_PATTERN = "dd.MM.yyyy";
    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_PATTERN);
    private LocalDate releasedDate;

    public Magazine(String title, String publisher, int quantity, LocalDate releasedDate) {
        super(title, publisher, quantity);
        this.releasedDate = releasedDate;
    }

    public Magazine(String title, String publisher, int quantity, long ean, LocalDate releasedDate) {
        super(title, publisher, quantity, ean);
        this.releasedDate = releasedDate;
    }

    public LocalDate getReleasedDate() {
        return releasedDate;
    }

    public void setReleasedDate(LocalDate releasedDate) {
        this.releasedDate = releasedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return Objects.equals(releasedDate, magazine.releasedDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), releasedDate);
    }

    @Override
    public String toString() {
        String withoutEan = super.toString() + " | data wydania: " + releasedDate.format(DATE_FORMATTER)
                + " | ilość na magazynie: " + getQuantity() + " egz.";
        String withEan = withoutEan + " | kod EAN: " + getEan() + "]";
        if(getEan() == 0) return withoutEan + "]";
        return withEan;
    }
}
