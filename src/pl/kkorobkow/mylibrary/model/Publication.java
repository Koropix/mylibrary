package pl.kkorobkow.mylibrary.model;

import pl.kkorobkow.mylibrary.model.borrow.Borrower;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public abstract class Publication implements Serializable, Comparable<Publication> {
    private static final long serialVersionUID = 20200224;
    private String title;
    private String publisher;
    private int quantity;
    private long ean;
    private List<Borrower> borrowers = new ArrayList<>();

    Publication(String title, String publisher, int quantity) {
        this.title = title;
        this.publisher = publisher;
        this.quantity = quantity;
    }

    Publication(String title, String publisher, int quantity, long ean) {
        this(title, publisher, quantity);
        this.ean = ean;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKey() {
        return title.toLowerCase();
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public long getEan() {
        return ean;
    }

    public void setEan(long ean) {
        this.ean = ean;
    }

    public List<Borrower> getBorrowers() {
        return borrowers;
    }

    public void setBorrowers(List<Borrower> borrowers) {
        this.borrowers = borrowers;
    }

    public List<Borrower> getSortedBorrowers() {
        return borrowers.stream()
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
    }

    void addBorrower(Borrower borrower) {
        borrowers.add(borrower);
    }

    void removeBorrower(Borrower borrower) {
        borrowers.remove(borrower);
    }

    public Borrower getFirstBorrowerContainsLibraryUser(LibraryUser libraryUser) {
        Borrower borrower = null;

        boolean borrowerFound = false;
        int i = 0;
        while (!borrowerFound && i < borrowers.size()) {
            Borrower tmpBorrower = borrowers.get(i);
            LibraryUser tmpLibraryUser = tmpBorrower.getLibraryUser();
            if (tmpLibraryUser.equals(libraryUser)) {
                borrower = tmpBorrower;
                borrowerFound = true;
            } else i++;
        }

        if (borrower != null) return borrower;
        throw new NoSuchElementException();
    }

    public void quantityReduce(int num) {
        quantity -= num;
    }

    public void quantityIncrease(int num) {
        quantity += num;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Publication that = (Publication) o;
        return ean == that.ean &&
                Objects.equals(title, that.title) &&
                Objects.equals(publisher, that.publisher);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, publisher, ean);
    }

    @Override
    public String toString() {
        return "\"" + title + "\" [wydawnictwo: " + publisher;
    }

    @Override
    public int compareTo(Publication p) {
        int titleCompare = title.compareToIgnoreCase(p.title);
        if (titleCompare != 0) return titleCompare;

        return publisher.compareToIgnoreCase(p.publisher);
    }

}
