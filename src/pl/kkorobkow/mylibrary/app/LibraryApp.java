package pl.kkorobkow.mylibrary.app;

import pl.kkorobkow.mylibrary.app.control.FileControl;

public class LibraryApp {
    private static final double VERSION = 1.9;

    public static void main(String[] args) {
        System.out.println("Witaj w programie myLibrary " + VERSION + '!');
        FileControl control = new FileControl();
        control.controlLoop();
    }
}
