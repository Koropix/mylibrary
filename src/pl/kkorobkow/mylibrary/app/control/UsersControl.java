package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.UserAlreadyExistsException;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.model.Library;
import pl.kkorobkow.mylibrary.model.LibraryUser;

import java.util.*;
import java.util.function.Supplier;

// klasa do przebudowy, jeśli będę chciał dodać inny typ użytkowników

class UsersControl extends Control {
    private UserControl userControl;

    UsersControl(ConsolePrinter printer, DataReader reader, Library library) {
        this.printer = printer;
        this.reader = reader;
        this.library = library;
        this.userControl = new UserControl(printer, reader, library);
    }

    void controlLoop() {
        UsersOption chosenOption = null;

        while (chosenOption != UsersOption.EXIT) {
            printer.printLine("\nWybierz jedną z dostępnych opcji:");
            printSpecificOptions();
            chosenOption = getSpecificOption();

            switch (chosenOption) {
                case EXIT:
                    break;
                case ADD_LIBRARY_USER:
                    createLibraryUserAndAddToLibrary();
                    break;
                case PRINT_LIBRARY_USERS:
                    printLibraryUsers();
                    break;
                case FIND_LIBRARY_USER:
                    findLibraryUser();
                    break;
                default:
                    printer.printErrorLine("Wyrabno nieistniejącą opcję.");
            }
        }
    }

    private UsersOption getSpecificOption() {
        Supplier<UsersOption> optionSupplier = () -> UsersOption.fromInt(reader.getInt());
        Class<UsersOption> enumSubclass = UsersOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(UsersOption.class);
    }

    private void createLibraryUserAndAddToLibrary() {
        LibraryUser user = reader.createNewLibraryUser();
        try {
            library.addLibraryUser(user);
            printer.printLine("Czytelnik [" + user + "] został poprawnie dodany do bazy.");
        } catch (UserAlreadyExistsException e) {
            printer.printErrorLine(e.getMessage());
        }
    }

    private void printLibraryUsers() {
        printer.printLine("Czytelnicy zarejestrowani w systemie:");
        Collection<LibraryUser> sortedLibraryUsers = library.getSortedLibraryUsers(Comparator.naturalOrder());
        printer.printLibraryUsers(sortedLibraryUsers);
    }

    private void findLibraryUser() {
        printer.printLine("Wprowadź nr PESEL czytelnika, którego chcesz odnaleźć:");
        Long pesel = reader.getPesel();
        Optional<LibraryUser> user = library.findLibraryUserByPesel(pesel);
        user.ifPresentOrElse(
                u -> {
                    printer.printLibraryUserInfo(u);
                    userControl.controlLoop(u);
                },
                () -> printer.printErrorLine("Czytelnik o nr. PESEL \"" + pesel + "\" nie istnieje w bazie danych.")
        );
    }

    private enum UsersOption {
        EXIT(0, "powrót do poprzedniego menu"),
        ADD_LIBRARY_USER(1, "dodanie nowego czytelnika"),
        PRINT_LIBRARY_USERS(2, "wyświetlenie istniejących czytelników"),
        FIND_LIBRARY_USER(3, "wyszukanie istniejącego czytelnika po nr. PESEL");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        UsersOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static UsersOption fromInt(int option) {
            boolean optionInRange = option >= UsersOption.MIN_VALUE && option <= UsersOption.MAX_VALUE;
            if (optionInRange) {
                UsersOption[] options = UsersOption.values();
                for (UsersOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }


}
