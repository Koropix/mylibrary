package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.PublicationAlreadyExistsException;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.model.Library;
import pl.kkorobkow.mylibrary.model.Publication;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Supplier;

class PublicationsControl extends Control {
    private PublicationControl publicationControl;
    private PublicationsImportControl publicationsImportControl;

    PublicationsControl(ConsolePrinter printer, DataReader reader, Library library) {
        this.printer = printer;
        this.reader = reader;
        this.library = library;
        this.publicationControl = new PublicationControl(printer, reader, library);
        this.publicationsImportControl = new PublicationsImportControl(printer, reader, library);
    }

    void controlLoop() {
        PublicationsOption chosenOption = null;

        while (chosenOption != PublicationsOption.EXIT) {
            printer.printLine("\nWybierz jedną z dostępnych opcji:");
            printSpecificOptions();
            chosenOption = getSpecificOption();

            switch (chosenOption) {
                case EXIT:
                    break;
                case ADD_BOOK:
                    createBookAndAddToLibrary();
                    break;
                case ADD_MAGAZINE:
                    createMagazineAndAddToLibrary();
                    break;
                case PRINT_BOOKS:
                    printBooks();
                    break;
                case PRINT_MAGAZINES:
                    printMagazines();
                    break;
                case FIND_PUBLICATION:
                    findPublication();
                    break;
                case IMPORT_PUBLICATIONS:
                    publicationsImportControl.controlLoop();
                    break;
                default:
                    printer.printErrorLine("Wyrabno nieistniejącą opcję.");
            }
        }
    }

    private PublicationsOption getSpecificOption() {
        Supplier<PublicationsOption> optionSupplier = () -> PublicationsOption.fromInt(reader.getInt());
        Class<PublicationsOption> enumSubclass = PublicationsOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(PublicationsOption.class);
    }

    private void createBookAndAddToLibrary() {
        createPublicationAndAddToLibrary(reader::createNewBook);
    }

    private void createMagazineAndAddToLibrary() {
        createPublicationAndAddToLibrary(reader::createNewMagazine);
    }

    // jako argument do tej metody przekazywać referencję do metody createNewBook()/Magazine() (z reader-a)
    private void createPublicationAndAddToLibrary(Supplier<Publication> supplier) {
        Publication publication = supplier.get();
        try {
            library.addPublication(publication);
            printer.printLine("Publikacja \"" + publication.getTitle() + "\" została poprawnie dodana do bazy.");
        } catch (PublicationAlreadyExistsException e) {
            printer.printErrorLine(e.getMessage());
        }
    }

    private void printBooks() {
        printPublications("książki", printer::printBooks);
    }

    private void printMagazines() {
        printPublications("magazyny", printer::printMagazines);
    }

    private void printPublications(String publicationsType,
                                   Consumer<Collection<Publication>> printMethod) {
        printer.printLine("Dostępne " + publicationsType + ":");
        Collection<Publication> sortedPublications = library.getSortedPublications(Comparator.naturalOrder());
        printMethod.accept(sortedPublications);
    }

    private void findPublication() {
        printer.printLine("Wprowadź tytuł lub fragment tytułu publikacji, którą chcesz odnaleźć:");
        String typedPhrase = reader.getString();
        Optional<Publication> pub = library.findPublicationByKey(typedPhrase);

        pub.ifPresentOrElse(
                p -> {
                    printer.printPublicationInfo(p);
                    publicationControl.controlLoop(p);
                },
                () -> findPublicationsByTypedPhrase(typedPhrase)
        );
    }

    private void findPublicationsByTypedPhrase(String typedPhrase) {
        final int maxResultsNumber = 25;
        Publication[] foundPublications = library.findPublicationsContainingPhrase(typedPhrase);

        if (foundPublications.length > 0 && foundPublications.length <= maxResultsNumber) {
            publicationsArrayIsCorrect(typedPhrase, foundPublications);
        } else if (foundPublications.length > maxResultsNumber) {
            printer.printErrorLine("Liczba publikacji zawierajacych w tytule frazę \"" + typedPhrase + "\" przekracza " +
                    "maksymalny limit (" + maxResultsNumber + ") - wprowadzana fraza powinna być bardziej precyzyjna.");
        } else {
            printer.printErrorLine("W bazie danych nie istnieje żadna publikacja zawierająca w tytule " +
                    "frazę \"" + typedPhrase + "\".");
        }
    }

    private void publicationsArrayIsCorrect(String partOfTitle, Publication[] foundPublications) {
        printer.printLine("Jeśli chodziło Ci o jedną z poniższych publikacji, wprowadź przypisany do niej nr " +
                "(jeśli nie wprowadź 0):");
        for (int i = 0; i < foundPublications.length; i++) {
            int indexForUser = i + 1;
            printer.printLine(indexForUser + " - " + foundPublications[i].toString());
        }
        int chosenIndex = reader.getIntFromGivenRange(0, foundPublications.length);

        if (chosenIndex != 0) {
            Publication foundPublication = foundPublications[chosenIndex - 1];
            printer.printPublicationInfo(foundPublication);
            publicationControl.controlLoop(foundPublication);
        } else {
            printer.printErrorLine("W bazie danych nie istnieje żadna inna publikacja zawierająca " +
                    "w tytule frazę \"" + partOfTitle + "\".");
        }
    }

    private enum PublicationsOption {
        EXIT(0, "powrót do poprzedniego menu"),
        ADD_BOOK(1, "dodanie nowej książki"),
        ADD_MAGAZINE(2, "dodanie nowego magazynu"),
        PRINT_BOOKS(3, "wyświetlenie dostępnych książek"),
        PRINT_MAGAZINES(4, "wyświetlenie dostępnych magazynów"),
        FIND_PUBLICATION(5, "wyszukanie istniejącej publikacji po tytule"),
        IMPORT_PUBLICATIONS(6, "import publikacji z zewnętrznego pliku CSV");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        PublicationsOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static PublicationsOption fromInt(int option) {
            boolean optionInRange = option >= PublicationsOption.MIN_VALUE && option <= PublicationsOption.MAX_VALUE;
            if (optionInRange) {
                PublicationsOption[] options = PublicationsOption.values();
                for (PublicationsOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }

}
