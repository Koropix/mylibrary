package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.DataImportException;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.io.file.SerializableFileManager;
import pl.kkorobkow.mylibrary.model.Library;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

public class FileControl extends Control {
    private SerializableFileManager fileManager;

    public FileControl() {
        printer = new ConsolePrinter();
        reader = new DataReader(printer);
        fileManager = new SerializableFileManager();
    }

    public void controlLoop() {
        boolean loopEnd = false;
        FileOption chosenOption = null;

        while (!loopEnd) {
            printer.printLine("\nWybierz jedną z dostępnych opcji:");
            printSpecificOptions();
            chosenOption = getSpecificOption();

            switch (chosenOption) {
                case EXIT:
                    loopEnd = exit();
                    break;
                case QUICK_LOAD:
                    loopEnd = quickLoad();
                    break;
                case CREATE_NEW:
                    loopEnd = createNew();
                    break;
                case LOAD:
                    loopEnd = load();
                    break;
                default:
                    printer.printErrorLine("Wyrabno nieistniejącą opcję.");
            }
        }

        if (chosenOption != FileOption.EXIT) {
            launchLibraryControl();
        }
    }

    private FileOption getSpecificOption() {
        Supplier<FileOption> optionSupplier = () -> FileOption.fromInt(reader.getInt());
        Class<FileOption> enumSubclass = FileOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(FileOption.class);
    }

    private boolean exit() {
        printer.printLine("Do widzenia!");
        reader.close();
        return true;
    }

    private boolean quickLoad() {
        try {
            library = fileManager.quickImportData();
            printer.printLine("Poprawnie wczytano poprzednią sesję.");
            return true;
        } catch (DataImportException e) {
            printer.printErrorLine(e.getMessage());
        }
        return false;
    }

    private boolean createNew() {
        String question = "Czy na pewno chcesz stworzyć nową bazę danych (tak/nie)?";
        boolean answer = confirmation(question);

        if (answer) {
            library = new Library();
            printer.printLine("Utworzono nową bazę danych.");
            return true;
        } else {
            printer.printLine("Nowa baza danych nie została utworzona.");
        }
        return false;
    }

    private boolean confirmation(String question) {
        if (SerializableFileManager.CURRENT_SESSION_FILE.exists()) {
            printer.printErrorLine("Uwaga, ryzyko utraty danych z poprzedniej sesji! Upewnij się czy poprzednia sesja " +
                    "została zapisana do zewnętrznego pliku.");
            printer.printLine(question);
            return reader.getAnswer();
        }
        return true;
    }

    private boolean load() {
        boolean result = false;

        if (fileManager.isSavesArrayEmpty()) {
            printer.printErrorLine("Folder \"" + SerializableFileManager.SAVES_DIRECTORY.getPath() + "\" jest pusty");
            return false;
        } else {
            String question = "Czy na pewno chcesz wczytać bazę danych z zewnętrznego pliku (tak/nie)?";
            boolean answer = confirmation(question);

            if (answer) {
                result = getSaveByNameAndLoad();
            } else {
                printer.printLine("Baza danych nie została wczytana.");
            }
        }
        return result;
    }

    private boolean getSaveByNameAndLoad() {
        printer.printLine("Lista dostępnych plików:");
        Arrays.stream(fileManager.getSortedSavesNamesArray()).forEach( f -> printer.printLine("\t - " + f));

        printer.printLine("\nPodaj nazwę pliku, z którego chcesz wczytać bazę danych:");
        String fileName = reader.getFileNameWithPathAndExtension();
        try {
            library = fileManager.importData(fileName);
            printer.printLine("Dane z pliku: \"" + fileName + "\" zostały poprawnie wczytane.");
            return true;
        } catch (DataImportException e) {
            printer.printErrorLine(e.getMessage());
        }
        return false;
    }

    private void launchLibraryControl() {
        LibraryControl libraryControl = new LibraryControl(fileManager, printer, reader, library);
        libraryControl.controlLoop();
    }

    private enum FileOption {
        EXIT(0, "wyjście z programu"),
        QUICK_LOAD(1, "wczytanie poprzedniej automatycznie zapisanej sesji"),
        CREATE_NEW(2, "utworzenie nowej bazy danych"),
        LOAD(3, "wczytanie poprzednio zapisanej bazy danych");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        FileOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static FileOption fromInt(int option) {
            boolean optionInRange = option >= FileOption.MIN_VALUE && option <= FileOption.MAX_VALUE;
            if (optionInRange) {
                FileOption[] options = FileOption.values();
                for (FileOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }


}
