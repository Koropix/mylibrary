package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.PublicationBorrowingException;
import pl.kkorobkow.mylibrary.exception.UnreturnedPublicationException;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.model.Library;
import pl.kkorobkow.mylibrary.model.LibraryUser;
import pl.kkorobkow.mylibrary.model.Publication;

import java.util.NoSuchElementException;
import java.util.function.Supplier;

class PublicationControl extends Control {
    private Publication publication;

    PublicationControl(ConsolePrinter printer, DataReader reader, Library library) {
        this.printer = printer;
        this.reader = reader;
        this.library = library;
    }

    void controlLoop(Publication publication) {
        this.publication = publication;
        boolean loopEnd = false;

        while (!loopEnd) {
            printer.printLine("\nMenu publikacji \"" + publication.getTitle() + "\":");
            printSpecificOptions();

            switch (getSpecificOption()) {
                case EXIT:
                    loopEnd = true;
                    break;
                case PRINT_INFO:
                    printInfo();
                    break;
                case BORROW_PUBLICATION:
                    borrowPublication();
                    break;
                case PUBLICATION_QUANTITY_INCREASE:
                    publicationQuantityIncrease();
                    break;
                case PUBLICATION_QUANTITY_REDUCE:
                    publicationQuantityReduce();
                    break;
                case DELETE_PUBLICATION:
                    loopEnd = deletePublication();
                    break;
                default:
                    printer.printErrorLine("Wyrabno nieistniejącą opcję.");
            }
        }
    }

    private PublicationOption getSpecificOption() {
        Supplier<PublicationOption> optionSupplier = () -> PublicationOption.fromInt(reader.getInt());
        Class<PublicationOption> enumSubclass = PublicationOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(PublicationOption.class);
    }

    private void printInfo() {
        printer.printPublicationInfo(publication);
    }

    private void borrowPublication() {
        boolean publicationStillAvailable = publication.getQuantity() > 0;
        if (publicationStillAvailable) {
            LibraryUser user;

            printer.printLine("Wprowadź nr. PESEL czytelnika wypożyczającego publikację:");
            long pesel = reader.getPesel();
            try {
                user = library.getLibraryUserFromOptionalByPesel(pesel);
                library.borrowPublicationToUser(publication, user);
                printer.printLine("Publikacja \"" + publication.getTitle() + "\" została poprawnie wypożyczona czytelnikowi "
                        + user + '.');
            } catch (NoSuchElementException | PublicationBorrowingException e) {
                printer.printErrorLine(e.getMessage());
            }
        } else {
            printer.printErrorLine("Wszystkie dostępne egzemplarze publikacji \"" + publication.getTitle()
                    + "\" są już wypożyczone.");
        }
    }

    private void publicationQuantityIncrease() {
        printer.printLine("Podaj o ile chcesz zwiększyć liczbę egzemplarzy publikacji:");
        int number = reader.getNonNegativeInt();
        publication.quantityIncrease(number);
        printer.printLine("Liczba dostępnych egzemplarzy publikacji \"" + publication.getTitle()
                + "\" została zwiększona o: " + number + '.');
    }

    private void publicationQuantityReduce() {
        printer.printLine("Podaj o ile chcesz zmniejszyć liczbę egzemplarzy publikacji:");
        int number = reader.getNonNegativeInt();
        if (publication.getQuantity() - number < 0) {
            printer.printErrorLine("Publikacja \"" + publication.getTitle() + "\" nie występuje na magazynie w liczbie "
                    + number + " egz.");
        } else {
            publication.quantityReduce(number);
            printer.printLine("Liczba dostępnych egzemplarzy publikacji \"" + publication.getTitle()
                    + "\" została zmniejszona o: " + number + '.');
        }
    }

    private boolean deletePublication() {
        printer.printErrorLine("Uwaga! Operacja nieodwracalna.");
        printer.printLine("Czy na pewno chcesz usunąć publikację (tak/nie)?");
        boolean answer = reader.getAnswer();

        if (answer) {
            try {
                library.removePublication(publication);
                printer.printLine("Publikacja została poprawnie usunięta.");
                return true;
            } catch (UnreturnedPublicationException e) {
                printer.printErrorLine(e.getMessage());
            }
        } else {
            printer.printLine("Publikacja nie została usunięta.");
        }
        return false;
    }

    private enum PublicationOption {
        EXIT(0, "powrót do poprzedniego menu"),
        PRINT_INFO(1, "ponowne wyświetlenie informacji o publikacji"),
        BORROW_PUBLICATION(2, "wypożyczenie publikacji"),
        PUBLICATION_QUANTITY_INCREASE(3, "zwiększenie liczby egzemplarzy publikacji"),
        PUBLICATION_QUANTITY_REDUCE(4, "zmniejszenie liczby egzemplarzy publikacji"),
        DELETE_PUBLICATION(9, "usunięcie publikacji");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        PublicationOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static PublicationOption fromInt(int option) {
            boolean optionInRange = option >= PublicationOption.MIN_VALUE && option <= PublicationOption.MAX_VALUE;
            if (optionInRange) {
                PublicationOption[] options = PublicationOption.values();
                for (PublicationOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }
}
