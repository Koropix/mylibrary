package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.DataExportException;
import pl.kkorobkow.mylibrary.exception.DataImportException;
import pl.kkorobkow.mylibrary.exception.InvalidDataException;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.io.csv.CsvImportManager;
import pl.kkorobkow.mylibrary.io.csv.ExampleCsvFileCreator;
import pl.kkorobkow.mylibrary.model.Library;

import java.util.Collection;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

class PublicationsImportControl extends Control {

    PublicationsImportControl(ConsolePrinter printer, DataReader reader, Library library) {
        this.printer = printer;
        this.reader = reader;
        this.library = library;
    }

    void controlLoop() {
        PublicationsImportOption chosenOption = null;

        while (chosenOption != PublicationsImportOption.EXIT) {
            printer.printLine("\nWybierz jedną z dostępnych opcji:");
            printSpecificOptions();
            chosenOption = getSpecificOption();

            switch (chosenOption) {
                case EXIT:
                    break;
                case IMPORT_PUBLICATIONS:
                    importPublications();
                    break;
                case GET_EXAMPLE_IMPORT_FILE:
                    getExampleImportFile();
                    break;
                default:
                    printer.printErrorLine("Wyrabno nieistniejącą opcję.");
            }
        }
    }

    private PublicationsImportOption getSpecificOption() {
        Supplier<PublicationsImportOption> optionSupplier = () -> PublicationsImportOption.fromInt(reader.getInt());
        Class<PublicationsImportOption> enumSubclass = PublicationsImportOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(PublicationsImportOption.class);
    }

    private void importPublications() {
        CsvImportManager csvImportManager = new CsvImportManager();
        printer.printLine("Podaj ścieżkę do pliku CSV:");
        try {
            String filePath = reader.getCsvFilePath();
            csvImportManager.importPublications(filePath, library);
            printImportSummaryMessage(csvImportManager);
        } catch (InvalidDataException | DataImportException e) {
            printer.printErrorLine(e.getMessage());
        }
    }

    private void printImportSummaryMessage(CsvImportManager csvImportManager) {
        Collection<String> smallDataImportExceptionsList = csvImportManager.getSortedSmallDataImportExceptionsList();
        if (smallDataImportExceptionsList.isEmpty()) {
            printer.printLine("Import publikacji przebiegł pomyślnie.");
        } else {
            printer.printErrorLine("Część publikacji nie została zaimportowana z uwagi na następujące błędy:");
            smallDataImportExceptionsList.forEach(printer::printErrorLine);
        }
    }

    private void getExampleImportFile() {
        printer.printLine("Podaj ścieżkę do folderu, w którym chcesz zapisać szablon pliku importu:");
        String directoryPath = reader.getString();
        ExampleCsvFileCreator csvFileCreator = new ExampleCsvFileCreator();
        try {
            csvFileCreator.createNew(directoryPath);
            printer.printLine("Plik \"" + directoryPath + '/' + ExampleCsvFileCreator.FILE_NAME
                    + "\" został zapisany pomyślnie.");
        } catch (DataExportException e) {
            printer.printErrorLine(e.getMessage());
        }
    }

    private enum PublicationsImportOption {
        EXIT(0, "powrót do poprzedniego menu"),
        IMPORT_PUBLICATIONS(1, "import publikacji"),
        GET_EXAMPLE_IMPORT_FILE(2, "pobranie szablonu pliku importu na dysk komputera " +
                "(zawiera przykładowe dane)");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        PublicationsImportOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static PublicationsImportOption fromInt(int option) {
            boolean optionInRange = option >= PublicationsImportOption.MIN_VALUE && option <= PublicationsImportOption.MAX_VALUE;
            if (optionInRange) {
                PublicationsImportOption[] options = PublicationsImportOption.values();
                for (PublicationsImportOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }


}
