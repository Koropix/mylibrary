package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.PublicationReturningException;
import pl.kkorobkow.mylibrary.exception.UnreturnedPublicationException;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.model.Library;
import pl.kkorobkow.mylibrary.model.LibraryUser;
import pl.kkorobkow.mylibrary.model.Publication;

import java.util.NoSuchElementException;
import java.util.function.Supplier;

class UserControl extends Control {
    private LibraryUser libraryUser;
    private String libraryUserName;

    UserControl(ConsolePrinter printer, DataReader reader, Library library) {
        this.printer = printer;
        this.reader = reader;
        this.library = library;
    }

    void controlLoop(LibraryUser libraryUser) {
        this.libraryUser = libraryUser;
        this.libraryUserName = libraryUser.getLastName() + ' ' + libraryUser.getFirstName();
        boolean loopEnd = false;

        while (!loopEnd) {
            printer.printLine("\nMenu czytelnika \"" + libraryUserName + "\":");
            printSpecificOptions();

            switch (getSpecificOption()) {
                case EXIT:
                    loopEnd = true;
                    break;
                case PRINT_INFO:
                    printInfo();
                    break;
                case RETURN_PUBLICATION:
                    returnPublication();
                    break;
                case DELETE_LIBRARY_USER:
                    loopEnd = deleteLibraryUser();
                    break;
                default:
                    printer.printErrorLine("Wyrabno nieistniejącą opcję.");
            }
        }
    }

    private UserOption getSpecificOption() {
        Supplier<UserOption> optionSupplier = () -> UserOption.fromInt(reader.getInt());
        Class<UserOption> enumSubclass = UserOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(UserOption.class);
    }

    private void printInfo() {
        printer.printLibraryUserInfo(libraryUser);
    }

    private void returnPublication() {
        boolean userIsBorrower = libraryUser.getBorrowedPublicationsNumber() > 0;
        if (userIsBorrower) {
            Publication pub;

            printer.printLine("Wprowadź tytuł zwracanej publikacji:");
            String title = reader.getString();
            try {
                pub = library.getPublicationFromOptionalByKey(title);
                library.returnPublicationFromUser(libraryUser, pub);
                printer.printLine("Publikacja \"" + pub.getTitle() + "\" została poprawnie zwrócona przez czytelnika "
                        + libraryUser + '.');
            } catch (NoSuchElementException | PublicationReturningException e) {
                printer.printErrorLine(e.getMessage());
            }
        } else {
            printer.printErrorLine("Użytkownik \"" + libraryUserName + "\" nie posiada żadnych wypożyczonych publikacji.");
        }
    }

    private boolean deleteLibraryUser() {
        printer.printErrorLine("Uwaga! Operacja nieodwracalna.");
        printer.printLine("Czy na pewno chcesz usunąć czytelnika (tak/nie)?");
        boolean answer = reader.getAnswer();

        if (answer) {
            try {
                library.removeLibraryUser(libraryUser);
                printer.printLine("Czytelnik został poprawnie usunięty.");
                return true;
            } catch (UnreturnedPublicationException e) {
                printer.printErrorLine(e.getMessage());
            }
        } else {
            printer.printLine("Czytelnik nie został usunięty.");
        }
        return false;
    }

    private enum UserOption {
        EXIT(0, "powrót do poprzedniego menu"),
        PRINT_INFO(1, "ponowne wyświetlenie informacji o czytelniku"),
        RETURN_PUBLICATION(2, "zwrot publikacji wypożyczonej przez czytelnika"),
        DELETE_LIBRARY_USER(9, "usunięcie istniejącego czytelnika");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        UserOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static UserOption fromInt(int option) {
            boolean optionInRange = option >= UserOption.MIN_VALUE && option <= UserOption.MAX_VALUE;
            if (optionInRange) {
                UserOption[] options = UserOption.values();
                for (UserOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }
}
