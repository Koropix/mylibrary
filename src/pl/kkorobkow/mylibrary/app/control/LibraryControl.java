package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.exception.*;
import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.io.file.SerializableFileManager;
import pl.kkorobkow.mylibrary.model.*;

import java.util.*;
import java.util.function.Supplier;

class LibraryControl extends Control {
    private SerializableFileManager fileManager;
    private PublicationsControl publicationsControl;
    private UsersControl usersControl;

    LibraryControl(SerializableFileManager fileManager, ConsolePrinter printer, DataReader reader, Library library) {
        this.fileManager = fileManager;
        this.printer = printer;
        this.reader = reader;
        this.library = library;
        publicationsControl = new PublicationsControl(printer, reader, library);
        usersControl = new UsersControl(printer, reader, library);
    }

    void controlLoop() {
        boolean programEnd = false;

        while (!programEnd) {
            printer.printLine("\nWybierz jedną z dostępnych opcji:");
            printSpecificOptions();

            switch (getSpecificOption()) {
                case EXIT:
                    programEnd = exitWithSaving();
                    break;
                case PUBLICATIONS_MENU:
                    publicationsControl.controlLoop();
                    break;
                case USERS_MENU:
                    usersControl.controlLoop();
                    break;
                case SAVE_AS:
                    saveAs();
                    break;
                case AUTOSAVE:
                    quickSave();
                    break;
                case EXIT_WITHOUT_SAVING:
                    programEnd = exitWithoutSaving();
                    break;
                default:
                    printer.printErrorLine("Wybrano nieistniejącą opcję.");
            }
        }
    }

    private MainOption getSpecificOption() {
        Supplier<MainOption> optionSupplier = () -> MainOption.fromInt(reader.getInt());
        Class<MainOption> enumSubclass = MainOption.class;
        return getOption(optionSupplier, enumSubclass);
    }

    private void printSpecificOptions() {
        printOptions(MainOption.class);
    }

    private boolean exit(boolean withSaving) {
        printer.printLine("Czy na pewno chcesz zamknąć program (tak/nie)?");
        boolean answer = reader.getAnswer();

        if (answer) {
            if (withSaving) {
                printer.printLine("Rozpoczęto zamykanie programu i zapis danych do pliku.");

                try {
                    fileManager.quickExportData(library);
                    printer.printLine("Dane zostały poprawnie zapisane.");
                } catch (DataExportException e) {
                    printer.printErrorLine(e.getMessage());
                }
            }
            printer.printLine("Do widzenia!");
            reader.close();
        } else {
            printer.printLine("Powrót do głównego menu.");
        }

        return answer;
    }

    private boolean exitWithoutSaving() {
        printer.printErrorLine("Uwaga! Dane z bieżącej sesji mogą zostać utracone.");
        return exit(false);
    }

    private boolean exitWithSaving() {
        printer.printErrorLine("Uwaga! Poprzednia sesja zostanie automatycznie nadpisana.");
        return exit(true);
    }

    private void saveAs() {
        printer.printLine("Podaj nazwę, pod którą chcesz zapisać bazę danych: (np.: \"example\")");
        String fileName = reader.getFileNameWithPathAndExtension();

        boolean savingConfirmation = getSavingConfirmation(fileName);
        if (savingConfirmation) {
            try {
                fileManager.exportData(library, fileName);
                printer.printLine("Dane zostały poprawnie zapisane do pliku: \"" + fileName + "\".");
            } catch (DataExportException e) {
                printer.printErrorLine(e.getMessage());
            }
        } else {
            printer.printLine("Plik \"" + fileName + "\" nie został napisany.");
        }
    }

    private boolean getSavingConfirmation(String fileNameWithPathAndExtension) {
        String[] savesNamesArray = fileManager.getSortedSavesNamesArray();

        String fileNameWithoutPath = fileNameWithPathAndExtension
                .replaceFirst(SerializableFileManager.SAVES_DIRECTORY.getPath() + '/', "");
        boolean fileAlreadyExists = Arrays.asList(savesNamesArray).contains(fileNameWithoutPath);

        if (fileAlreadyExists) {
            printer.printErrorLine("Uwaga! Plik \"" + fileNameWithPathAndExtension + "\" już istnieje.");
            printer.printLine("Czy chcesz go nadpisać (tak/nie)?");
            return reader.getAnswer();
        }
        return true;
    }

    private void quickSave() {
        printer.printLine("Rozpoczęto nadpisywanie bieżącej sesji.");
        try {
            fileManager.quickExportData(library);
            printer.printLine("Bieżąca sesja została poprawnie nadpisana.");
        } catch (DataExportException e) {
            printer.printErrorLine(e.getMessage());
        }
    }

    private enum MainOption {
        EXIT(0, "wyjście i autozapis stanu programu"),
        PUBLICATIONS_MENU(1, "zarządzanie publikacjami"),
        USERS_MENU(2, "zarządzanie użytkownikami"),
        SAVE_AS(7, "zapis bazy danych do zewnętrznego pliku"),
        AUTOSAVE(8, "autozapis bieżącej sesji"),
        EXIT_WITHOUT_SAVING(9, "wyjście z programu bez zapisywania");

        private static final int MIN_VALUE = EXIT.value;
        private static final int MAX_VALUE = values()[values().length - 1].value;
        private int value;
        private String description;

        MainOption(int value, String description) {
            this.value = value;
            this.description = description;
        }

        private static MainOption fromInt(int option) {
            boolean optionInRange = option >= MainOption.MIN_VALUE && option <= MainOption.MAX_VALUE;
            if (optionInRange) {
                MainOption[] options = MainOption.values();
                for (MainOption opt : options) {
                    if (option == opt.value) {
                        return opt;
                    }
                }
            }
            throw new NoSuchElementException("Brak opcji o id: " + option + '.');
        }

        @Override
        public String toString() {
            return value + " - " + description;
        }
    }
}
