package pl.kkorobkow.mylibrary.app.control;

import pl.kkorobkow.mylibrary.io.ConsolePrinter;
import pl.kkorobkow.mylibrary.io.DataReader;
import pl.kkorobkow.mylibrary.model.Library;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.function.Supplier;

abstract class Control {
    ConsolePrinter printer;
    DataReader reader;
    Library library;

    <T extends Enum<T>> T getOption(Supplier<? extends T> optionSupplier, Class<T> enumSubclass) {
        T result = null;
        boolean error = true;

        while (error) {
            try {
                result = optionSupplier.get();
                error = false;
            } catch (NoSuchElementException e) {
                printer.printErrorLine(e.getMessage());
                printer.printLine("Wybierz opcję ponownie:");
                printOptions(enumSubclass);
            }
        }
        return result;
    }

    <T extends Enum<T>> void printOptions(Class<T> enumSubclass) {
        Arrays.stream(enumSubclass.getEnumConstants())
                .forEach(o -> printer.printLine(o.toString()));
    }
}
