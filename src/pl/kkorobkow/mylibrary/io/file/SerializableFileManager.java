package pl.kkorobkow.mylibrary.io.file;

import pl.kkorobkow.mylibrary.exception.DataExportException;
import pl.kkorobkow.mylibrary.exception.DataImportException;
import pl.kkorobkow.mylibrary.model.Library;

import java.io.*;
import java.util.Arrays;

public class SerializableFileManager {
    private static final String DIRECTORY_PATH = "saves";
    public static final File SAVES_DIRECTORY = new File(DIRECTORY_PATH);
    public static final String FILE_EXTENSION = ".mylibrary";
    private static final String CURRENT_SESSION_FILE_NAME = "CurrentSession" + FILE_EXTENSION;
    public static final File CURRENT_SESSION_FILE = new File(CURRENT_SESSION_FILE_NAME);

    public SerializableFileManager() {
        if (!SAVES_DIRECTORY.exists()) {
            SAVES_DIRECTORY.mkdir();
        }
    }

    private File[] getSavesArray() {
        FileFilter myLibraryFileFilter = f -> !f.isDirectory() && f.getPath().endsWith(FILE_EXTENSION);
        return SAVES_DIRECTORY.listFiles(myLibraryFileFilter);
    }

    public boolean isSavesArrayEmpty() {
        File[] savesArray = getSavesArray();
        return savesArray.length < 1;
    }

    public String[] getSortedSavesNamesArray() {
        File[] savesArray = getSavesArray();
        return Arrays.stream(savesArray)
                .map(File::getName)
                .sorted()
                .toArray(String[]::new);
    }

    public void quickExportData(Library library) {
        exportData(library, CURRENT_SESSION_FILE_NAME);
    }

    public void exportData(Library library, String fileName) {
        try (var oos = new ObjectOutputStream(new FileOutputStream(fileName))) {
            oos.writeObject(library);
        } catch (FileNotFoundException e) {
            throw new DataExportException("Nie udało się utworzyć ani odnaleźć pliku \"" + fileName + '"');
        } catch (IOException e) {
            throw new DataExportException("Wystapił nieznany błąd eksportu, bazy danych nie udało się zapisać do pliku.");
        }
    }

    public Library quickImportData() {
        return importData(CURRENT_SESSION_FILE_NAME);
    }

    public Library importData(String fileName) {
        Library library;
        try (var ois = new ObjectInputStream(new FileInputStream(fileName))) {
            library = (Library) ois.readObject();
        } catch (FileNotFoundException e) {
            if (fileName.equals(CURRENT_SESSION_FILE_NAME))
                throw new DataImportException("Brak danych z poprzedniej sesji.");
            else
                throw new DataImportException("Nie odnaleziono pliku \"" + fileName + "\".");
        } catch (ClassNotFoundException e) {
            throw new DataImportException("Plik \"" + fileName + "\" posiada nieprawidłową strukturę.");
        } catch (IOException e) {
            throw new DataImportException("Wystapił nieznany błąd importu.");
        }
        return library;
    }
}
