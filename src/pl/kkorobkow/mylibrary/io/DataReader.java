package pl.kkorobkow.mylibrary.io;

import pl.kkorobkow.mylibrary.exception.InvalidDataException;
import pl.kkorobkow.mylibrary.io.file.SerializableFileManager;
import pl.kkorobkow.mylibrary.model.Book;
import pl.kkorobkow.mylibrary.model.LibraryUser;
import pl.kkorobkow.mylibrary.model.Magazine;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.function.Supplier;

public class DataReader {
    private Scanner sc = new Scanner(System.in);
    private ConsolePrinter printer;
    private static final String[] FILE_NAME_FORBIDDEN_CHARS = {"\\", "/", "*", "?", "\"", "<", ">", "|"};

    public DataReader(ConsolePrinter printer) {
        this.printer = printer;
    }

    public void close() {
        sc.close();
    }

    private <T extends Number> T getNumber(Supplier<T> supplier) {
        T result = null;
        boolean error = true;

        while (error) {
            try {
                result = supplier.get();
                error = false;
            } catch (InputMismatchException e) {
                printer.printErrorLine("Podano wartość nieodpowiedniego typu lub za długą. Spróbuj ponownie:");
            } finally {
                sc.nextLine();
            }
        }
        return result;
    }

    public int getInt() {
        return getNumber(sc::nextInt);
    }

    public int getIntFromGivenRange(int startInclusive, int endInclusive) {
        int result = 0;
        boolean error = true;
        while (error) {
            result = getInt();
            if (result >= startInclusive && result <= endInclusive) {
                error = false;
            } else {
                printer.printErrorLine("Liczba " + result + " wykracza poza możliwy zakres. Spróbuj ponownie:");
            }
        }
        return result;
    }

    public int getNonNegativeInt() {
        int result;
        while ((result = getInt()) < 0) {
            printer.printErrorLine("Podawana liczba nie może być ujemna. Spróbuj jeszcze raz:");
        }
        return result;
    }

    private long getLong() {
        return getNumber(sc::nextLong);
    }

    private long getNonNegativeLong() {
        long result;
        while ((result = getLong()) < 0) {
            printer.printErrorLine("Podawana liczba nie może być ujemna. Spróbuj jeszcze raz:");
        }
        return result;
    }

    public long getPesel() {
        int peselLength = 11;
        int resultLength;

        long result;
        do {
            result = getNonNegativeLong();
            resultLength = Long.toString(result).length();

            if (resultLength > peselLength)
                printer.printErrorLine("Podany nr jest za długi (powinien składać się z 11 cyfr). Spróbuj jeszcze raz:");
            else if (resultLength < peselLength)
                printer.printErrorLine("Podany nr jest za krótki (powinien składać się z 11 cyfr). Spróbuj jeszcze raz:");

        } while (resultLength != peselLength);

        return result;
    }

    public String getString() {
        return sc.nextLine();
    }

    public String getFileNameWithPathAndExtension() {
        String result = null;
        boolean error = true;

        while (error) {
            result = getString();
            error = checkIsFileNameHasForbiddenChars(result);
            if (error)
                printer.printLine("Spróbuj jeszcze raz:");
        }

        result = SerializableFileManager.SAVES_DIRECTORY.getPath() + '/' + result;

        if (result.endsWith(SerializableFileManager.FILE_EXTENSION))
            return result;
        else
            return result + SerializableFileManager.FILE_EXTENSION;
    }

    private boolean checkIsFileNameHasForbiddenChars(String charsSequence) {
        for (String forbiddenChar : FILE_NAME_FORBIDDEN_CHARS) {
            if (charsSequence.contains(forbiddenChar)) {
                printer.printErrorLine("Nazwa pliku nie może zawierać następujących znaków: "
                        + getSequenceOfFileNameForbiddenChars());
                return true;
            }
        }
        return false;
    }

    private String getSequenceOfFileNameForbiddenChars() {
        StringBuilder forbiddenChars = new StringBuilder();
        for (String forbiddenChar : FILE_NAME_FORBIDDEN_CHARS) {
            forbiddenChars.append(forbiddenChar);
            forbiddenChars.append(" ");
        }
        return forbiddenChars.deleteCharAt(forbiddenChars.length() - 1).toString();
    }

    public String getCsvFilePath() {
        String filePath = getString();
        int extensionIndexStart = filePath.lastIndexOf('.');
        String fileExtension;

        try {
            fileExtension = filePath.substring(extensionIndexStart).toLowerCase();
        } catch (StringIndexOutOfBoundsException e) {
            throw new InvalidDataException("Plik ze wskazanej ścieżki nie posiada żadnego rozszerzenia.");
        }

        if (fileExtension.equals(".csv")) {
            return filePath;
        }
        throw new InvalidDataException("Plik ze wskazanej ścieżki posiada nieprawidłowe rozszerzenie (" + fileExtension + ").");
    }

    public boolean getAnswer() {
        final String yes = "tak";
        final String no = "nie";

        boolean answer = false;
        boolean error = true;

        while (error) {
            switch (getString().toLowerCase()) {
                case yes:
                    answer = true;
                    error = false;
                    break;
                case no:
                    error = false;
                    break;
                default:
                    printer.printErrorLine("Podano nieprawidłową odpowiedź. Spróbuj jeszcze raz:");
            }
        }

        return answer;
    }

    public Book createNewBook() {
        printer.printLine("Podaj tytuł książki:");
        String title = getString();
        printer.printLine("Podaj nazwę wydawnictwa:");
        String publisher = getString();
        printer.printLine("Podaj imię i nazwisko autora:");
        String author = getString();
        printer.printLine("Podaj rok wydania:");
        Year releasedYear = Year.of(getInt());
        printer.printLine("Podaj liczbę stron:");
        int pagesNumber = getNonNegativeInt();
        printer.printLine("Podaj liczbę egzemplarzy:");
        int quantity = getNonNegativeInt();
        printer.printLine("Podaj kod EAN (lub wpisz 0, jeśli nie chcesz go wprowadzać):");
        long ean = getNonNegativeLong();

        if (ean == 0) return new Book(title, publisher, quantity, author, releasedYear, pagesNumber);
        return new Book(title, publisher, quantity, ean, author, releasedYear, pagesNumber);
    }

    public Magazine createNewMagazine() {
        printer.printLine("Podaj tytuł oraz numer magazynu:");
        String title = getString();
        printer.printLine("Podaj nazwę wydawnictwa:");
        String publisher = getString();
        printer.printLine("Podaj datę wydania (w formacie "
                + Magazine.DATE_PATTERN.replaceAll("y", "r") + "):");
        LocalDate releasedDate = getLocalDate();
        printer.printLine("Podaj liczbę egzemplarzy:");
        int quantity = getNonNegativeInt();
        printer.printLine("Podaj kod EAN (lub wpisz 0, jeśli nie chcesz go wprowadzać):");
        long ean = getNonNegativeLong();

        if (ean == 0) return new Magazine(title, publisher, quantity, releasedDate);
        return new Magazine(title, publisher, quantity, ean, releasedDate);
    }

    private LocalDate getLocalDate() {
        LocalDate date = null;
        boolean error = true;

        while (error) {
            try {
                date = LocalDate.parse(getString(), Magazine.DATE_FORMATTER);
                error = false;
            } catch (DateTimeParseException e) {
                printer.printErrorLine("Podano datę niezgodną z obowiązującym formatem ("
                        + Magazine.DATE_PATTERN.replaceAll("y", "r") + "). Spróbuj ponownie.");
            }
        }
        return date;
    }

    public LibraryUser createNewLibraryUser() {
        printer.printLine("Podaj imię czytelnika:");
        String firstName = getString();
        printer.printLine("Podaj nazwisko czytelnika:");
        String lastName = getString();
        printer.printLine("Podaj nr PESEL czytelnika:");
        long pesel = getPesel();

        return new LibraryUser(firstName, lastName, pesel);
    }

}
