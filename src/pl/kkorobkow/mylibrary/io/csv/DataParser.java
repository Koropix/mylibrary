package pl.kkorobkow.mylibrary.io.csv;

import pl.kkorobkow.mylibrary.exception.InvalidDataException;
import pl.kkorobkow.mylibrary.model.Book;
import pl.kkorobkow.mylibrary.model.Magazine;

import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeParseException;
import java.util.Arrays;
import java.util.function.Supplier;

class DataParser {

    String parseType(String line) {
        String InvalidTypeMessage = "Błędne dane w kolumnie \"" + CsvFileColumnName.TYPE + "\".";
        String result = line.toUpperCase();
        if (result.equals(Magazine.TYPE) || result.equals(Book.TYPE)) {
            return result;
        }
        throw new InvalidDataException(InvalidTypeMessage);
    }

    Year parseReleasedYear(String line) {
        String[] tmp = CsvFileColumnName.REALISED_YEAR_QUANTITY.toString().split(" / ");
        String releasedYear = tmp[0];

        String numberFormatExceptionMessage = "Błędne lub błędnie sformatowane dane w kolumnie \"" + releasedYear + "\".";
        try {
            int yearNumber = Integer.parseInt(line);
            return Year.of(yearNumber);
        } catch (NumberFormatException e) {
            throw new InvalidDataException(numberFormatExceptionMessage);
        }
    }

    LocalDate parseReleasedDate(String line) {
        String[] tmp = CsvFileColumnName.AUTHOR_REALISED_DATE.toString().split(" / ");
        String releasedDate = tmp[1];

        String dateTimeParseExceptionMessage = "Błędne lub błędnie sformatowane dane w kolumnie \"" + releasedDate + "\".";
        try {
            return LocalDate.parse(line, Magazine.DATE_FORMATTER);
        } catch (DateTimeParseException e) {
            throw new InvalidDataException(dateTimeParseExceptionMessage);
        }
    }

    int parsePagesNumber(String line) {
        String[] tmp = CsvFileColumnName.PAGES_NUMBER_EAN.toString().split(" / ");
        String pagesNumber = tmp[0];

        String negativeNumberMessage = "Ujemna liczba w kolumnie \"" + pagesNumber + "\".";
        String numberFormatExceptionMessage = "Błędny typ danych w kolumnie \"" + pagesNumber + "\".";
        return parseNonNegativeInt(line, negativeNumberMessage, numberFormatExceptionMessage);
    }

    int parseQuantity(String line) {
        String quantity = Arrays.stream(CsvFileColumnName.QUANTITY.toString().split(" "))
                .limit(2).reduce("", (s1, s2) -> s1 + s2 + " ").trim();
        String negativeNumberMessage = "Ujemna liczba w kolumnie \"" + quantity + "\".";
        String numberFormatExceptionMessage = "Błędny typ danych w kolumnie \"" + quantity + "\".";
        return parseNonNegativeInt(line, negativeNumberMessage, numberFormatExceptionMessage);
    }

    private int parseNonNegativeInt(String line, String negativeNumberMessage, String numberFormatExceptionMessage) {
        Supplier<Integer> supplier = () -> Integer.parseInt(line);
        return parseNonNegativeNumber(supplier, negativeNumberMessage, numberFormatExceptionMessage);
    }

    long parseEan(String line) {
        String ean = Arrays.stream(CsvFileColumnName.EAN.toString().split(" "))
                .limit(2).reduce("", (s1, s2) -> s1 + s2 + " ").trim();
        String negativeNumberMessage = "Ujemna liczba w kolumnie \"" + ean + "\".";
        String numberFormatExceptionMessage = "Błędny typ danych w kolumnie \"" + ean + "\".";
        return parseNonNegativeLong(line, negativeNumberMessage, numberFormatExceptionMessage);
    }

    private long parseNonNegativeLong(String line, String negativeNumberMessage, String numberFormatExceptionMessage) {
        Supplier<Long> supplier = () -> Long.parseLong(line);
        return parseNonNegativeNumber(supplier, negativeNumberMessage, numberFormatExceptionMessage);
    }

    private <N extends Number> N parseNonNegativeNumber(Supplier<N> supplier, String negativeNumberMessage,
                                                        String numberFormatExceptionMessage) {
        try {
            N result = supplier.get();
            long tmpResult = Long.parseLong(result.toString());
            if (tmpResult < 0) {
                throw new InvalidDataException(negativeNumberMessage);
            }
            return result;
        } catch (NumberFormatException e) {
            throw new InvalidDataException(numberFormatExceptionMessage);
        }
    }
}
