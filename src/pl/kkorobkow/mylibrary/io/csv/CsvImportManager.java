package pl.kkorobkow.mylibrary.io.csv;

import pl.kkorobkow.mylibrary.exception.DataImportException;
import pl.kkorobkow.mylibrary.exception.InvalidDataException;
import pl.kkorobkow.mylibrary.exception.PublicationAlreadyExistsException;
import pl.kkorobkow.mylibrary.exception.SmallDataImportException;
import pl.kkorobkow.mylibrary.model.Book;
import pl.kkorobkow.mylibrary.model.Library;
import pl.kkorobkow.mylibrary.model.Magazine;
import pl.kkorobkow.mylibrary.model.Publication;

import java.io.*;
import java.time.LocalDate;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class CsvImportManager {
    static final String SEPARATOR = ";";
    private DataParser dataParser = new DataParser();
    private Collection<String> smallDataImportExceptionsList = new ArrayList<>();

    public Collection<String> getSortedSmallDataImportExceptionsList() {
        return smallDataImportExceptionsList.stream()
                .sorted(Comparator.naturalOrder())
                .collect(Collectors.toList());
    }

    public void importPublications(String filePath, Library library) throws DataImportException {
        String[] lines = getLines(filePath);
        int numberOfHeaders = 1;

        if (lines.length > numberOfHeaders) {
            Arrays.stream(lines)
                    .skip(numberOfHeaders)
                    .parallel()
                    .forEach(l -> getPublicationFromLineAndAddToLibrary(l, library));
        } else {
            throw new DataImportException("Plik jest pusty albo zawiera dane wyłącznie w sekcji nagłówka (pierwszy wiersz).");
        }
    }

    private String[] getLines(String filePath) {
        try (
                var fis = new FileInputStream(filePath);
                var isr = new InputStreamReader(fis, "Windows-1250");
                var br = new BufferedReader(isr)
        ) {
            return br.lines().toArray(String[]::new);
        } catch (FileNotFoundException e) {
            throw new DataImportException("Nie udało się odnaleźć pliku o ścieżce: " + filePath);
        } catch (IOException e) {
            throw new DataImportException("Wystapił nieznany błąd importu.");
        }
    }

    private void getPublicationFromLineAndAddToLibrary(String line, Library library) {
        String[] publicationFields = line.split(SEPARATOR);
        final int minimalLengthToLaunch = 2;

        if (publicationFields.length >= minimalLengthToLaunch) {
            String potentialType = publicationFields[0];
            String potentialTitle = publicationFields[1];

            try {
                String publicationType = checkPublicationType(potentialType, potentialTitle);
                if (publicationType.equals(Book.TYPE)) {
                    getBookAndAddToLibrary(publicationFields, library);
                }
                if (publicationType.equals(Magazine.TYPE)) {
                    getMagazineAndAddToLibrary(publicationFields, library);
                }
            } catch (SmallDataImportException e) {
                smallDataImportExceptionsList.add(e.getMessage());
            }
        }
    }

    private String checkPublicationType(String type, String title) {
        try {
            return dataParser.parseType(type);
        } catch (InvalidDataException e) {
            throw new SmallDataImportException("Import publikacji o tytule \"" + title + "\" nieudany. " +
                    "Publikacja posiada nieprawidłowe dane [" + e.getMessage() + "].");
        }
    }

    private void getBookAndAddToLibrary(String[] publicationFields, Library library) {
        Supplier<Book> supplier = () -> getBookFromLine(publicationFields);
        getPublicationAndAddToLibrary(supplier, library);
    }

    private void getMagazineAndAddToLibrary(String[] publicationFields, Library library) {
        Supplier<Magazine> supplier = () -> getMagazineFromLine(publicationFields);
        getPublicationAndAddToLibrary(supplier, library);
    }

    private <P extends Publication> void getPublicationAndAddToLibrary(Supplier<P> supplier, Library library) {
        P publication = supplier.get();
        try {
            library.addPublication(publication);
        } catch (PublicationAlreadyExistsException e) {
            throw new SmallDataImportException("Import publikacji o tytule \"" + publication.getTitle()
                    + "\" nieudany " + "[" + e.getMessage() + "].");
        }
    }

    private Book getBookFromLine(String[] publicationFields) {
        Supplier<Book> supplier = () -> {
            String title = publicationFields[1];
            String publisher = publicationFields[2];
            String author = publicationFields[3];
            Year releasedYear = dataParser.parseReleasedYear(publicationFields[4]);
            int pagesNumber = dataParser.parsePagesNumber(publicationFields[5]);
            int quantity = dataParser.parseQuantity(publicationFields[6]);

            try {
                long ean = dataParser.parseEan(publicationFields[7]);
                return new Book(title, publisher, quantity, ean, author, releasedYear, pagesNumber);
            } catch (IndexOutOfBoundsException e) {
                return new Book(title, publisher, quantity, author, releasedYear, pagesNumber);
            }
        };
        return getPublicationFromLine(publicationFields, supplier);
    }

    private Magazine getMagazineFromLine(String[] publicationFields) {
        Supplier<Magazine> supplier = () -> {
            String title = publicationFields[1];
            String publisher = publicationFields[2];
            LocalDate releasedDate = dataParser.parseReleasedDate(publicationFields[3]);
            int quantity = dataParser.parseQuantity(publicationFields[4]);

            try {
                long ean = dataParser.parseEan(publicationFields[5]);
                return new Magazine(title, publisher, quantity, ean, releasedDate);
            } catch (IndexOutOfBoundsException e) {
                return new Magazine(title, publisher, quantity, releasedDate);
            }
        };
        return getPublicationFromLine(publicationFields, supplier);
    }

    private <P extends Publication> P getPublicationFromLine(String[] publicationFields, Supplier<P> supplier) {
        String title = publicationFields[1];

        try {
            return supplier.get();
        } catch (IndexOutOfBoundsException e) {
            throw new SmallDataImportException("Import publikacji o tytule \"" + title + "\" nieudany. " +
                    "Publikacja posiada zbyt mało wypełnionych pól.");
        } catch (InvalidDataException e) {
            throw new SmallDataImportException("Import publikacji o tytule \"" + title + "\" nieudany. " +
                    "Publikacja posiada nieprawidłowe dane [" + e.getMessage() + "].");
        }

    }
}
