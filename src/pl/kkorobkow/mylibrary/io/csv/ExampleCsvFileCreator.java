package pl.kkorobkow.mylibrary.io.csv;

import pl.kkorobkow.mylibrary.exception.DataExportException;
import pl.kkorobkow.mylibrary.model.Book;
import pl.kkorobkow.mylibrary.model.Magazine;

import java.io.*;
import java.util.Arrays;

public class ExampleCsvFileCreator {
    public static final String FILE_NAME = "MyLibraryCsvFileTemplate.csv";

    public void createNew(String directoryPath) throws DataExportException {
        File directory = new File(directoryPath);
        boolean directoryExists = directory.exists();

        if (directoryExists) {
            File file = new File(directoryPath + '/' + FILE_NAME);
            try (
                    var fos = new FileOutputStream(file);
                    var osw = new OutputStreamWriter(fos, "Windows-1250");
                    var bw = new BufferedWriter(osw)
            ) {
                bw.write(createHeaderLine());
                bw.newLine();
                bw.write(createExampleBook());
                bw.newLine();
                bw.write(createExampleMagazine());
            } catch (IOException e) {
                throw new DataExportException("Wystapił nieznany błąd, nie udało się wygenerować szablonu pliku importu.");
            }
        } else {
            throw new DataExportException("Folder o wskazanej ścieżce nie istnieje na Twoim komputerze.");
        }
    }

    private String createHeaderLine() {
        String[] columnsNames = Arrays.stream(CsvFileColumnName.values())
                .map(CsvFileColumnName::toString).toArray(String[]::new);
        return reduceStringArrayToOneString(columnsNames);
    }

    private String createExampleBook() {
        String type = Book.TYPE;
        String title = "Wiedźmin. Ostatnie życzenie";
        String publisher = "Supernowa";
        String author = "Andrzej Sapkowski";
        String releasedYear = "2014";
        String pagesNumber = "332";
        String quantity = "0";
        String ean = "9788375780635";
        String[] bookFields = {type, title, publisher, author, releasedYear, pagesNumber, quantity, ean};

        return reduceStringArrayToOneString(bookFields);
    }

    private String createExampleMagazine() {

        String type = Magazine.TYPE;
        String title = "Książki. Magazyn do czytania 1/2020";
        String publisher = "Agora";
        String releasedDate = Magazine.DATE_PATTERN
                .replace("dd", "01")
                .replace("MM", "01")
                .replace("yyyy", "2020");
        String quantity = "0";
        String ean = "977208338800902";
        String[] magazineFields = {type, title, publisher, releasedDate, quantity, ean};

        return reduceStringArrayToOneString(magazineFields);
    }

    private String reduceStringArrayToOneString(String[] strings) {
        var sb = new StringBuilder();
        for (String str : strings) {
            sb.append(str);
            sb.append(CsvImportManager.SEPARATOR);
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

}
