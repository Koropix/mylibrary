package pl.kkorobkow.mylibrary.io.csv;

enum CsvFileColumnName {
    TYPE("TYP PUBLIKACJI"),
    TITLE("TYTUŁ PUBLIKACJI"),
    PUBLISHER("WYDAWNICTWO PUBLIKACJI"),
    AUTHOR_REALISED_DATE("AUTOR KSIĄŻKI / DATA WYDANIA MAGAZYNU"),
    REALISED_YEAR_QUANTITY("ROK WYDANIA KSIĄŻKI / LICZBA EGZEMPLARZY MAGAZYNU"),
    PAGES_NUMBER_EAN("LICZBA STRON KSIĄŻKI / KOD EAN MAGAZYNU"),
    QUANTITY("LICZBA EGZEMPLARZY KSIĄŻKI"),
    EAN("KOD EAN KSIĄŻKI");

    private String name;

    CsvFileColumnName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
