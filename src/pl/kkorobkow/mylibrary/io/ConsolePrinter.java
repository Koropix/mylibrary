package pl.kkorobkow.mylibrary.io;

import pl.kkorobkow.mylibrary.model.*;
import pl.kkorobkow.mylibrary.model.borrow.BorrowedPublication;
import pl.kkorobkow.mylibrary.model.borrow.Borrower;

import java.util.Collection;
import java.util.Objects;

public class ConsolePrinter {

    public void printLine(String line) {
        System.out.println(line);
    }

    public void printErrorLine(String line) {
        System.err.println(line);
    }

    public void printBooks(Collection<Publication> publications) {
        printBooksOrMagazines(publications, Book.class);
    }

    public void printMagazines(Collection<Publication> publications) {
        printBooksOrMagazines(publications, Magazine.class);
    }

    private void printBooksOrMagazines(Collection<Publication> publications, Class<? extends Publication> publicationSubclass) {
        long booksNumber = publications.stream()
                .filter(p -> p.getClass() == publicationSubclass)
                .map(Publication::toString)
                .peek(this::printLine)
                .count();
        if (booksNumber == 0)
            printErrorLine("Brak tytułów.");
    }

    public void printLibraryUsers(Collection<LibraryUser> libraryUsers) {
        long libraryUsersNumber =
                libraryUsers.stream()
                        .filter(Objects::nonNull)
                        .map(LibraryUser::toString)
                        .peek(this::printLine)
                        .count();
        if (libraryUsersNumber == 0)
            printErrorLine("Brak zarejestrowanych czytelników.");
    }

    public void printPublicationInfo(Publication publication) {
        printLine("Informacje na temat publikacji:");
        printLine(publication.toString());
        printBorrowers(publication.getSortedBorrowers());
    }

    private void printBorrowers(Collection<Borrower> borrowers) {
        if (!borrowers.isEmpty()) {
            printLine("Czytelnicy aktualnie wypożyczający tę publikację:");
            borrowers.forEach(b -> printLine(b.toString()));
        }
    }

    public void printLibraryUserInfo(LibraryUser libraryUser) {
        printLine("Informacje na temat czytelnika:");
        printLine(libraryUser.toString());
        printBorrowedPublications(libraryUser.getSortedBorrowedPublications());
        printPublicationHistory(libraryUser.getSortedPublicationHistory());
    }

    private void printBorrowedPublications(Collection<BorrowedPublication> borrowedPublications) {
        if (!borrowedPublications.isEmpty()) {
            printLine("Publikacje wypożyczone przez czytelnika:");
            borrowedPublications.forEach(bp -> printLine(bp.toString()));
        }
    }

    private void printPublicationHistory(Collection<BorrowedPublication> publicationHistory) {
        if (!publicationHistory.isEmpty()) {
            printLine("Publikacje wypożyczane przez czytelnika w przeszłości:");
            publicationHistory.forEach(ph -> printLine(ph.toString()));
        }
    }
}
